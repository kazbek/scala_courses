name := "BitcoinArbitrage"

version := "0.1"

scalaVersion := "2.12.8"


libraryDependencies ++= Seq(

  // Simplified Http
  "org.scalaj" %% "scalaj-http" % "2.3.0",

  // Spray Json 
  "io.spray" %%  "spray-json" % "1.3.5"
  
)