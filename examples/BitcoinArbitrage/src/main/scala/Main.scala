import scalaj.http._
import spray.json.DefaultJsonProtocol._
import spray.json._

import scala.collection.mutable


case class RowData(from: String, to: String, rate: BigDecimal)

case class Arrow(from: String, to: String)
case class Accumulator(rate: BigDecimal, arrows: Vector[Arrow])
case class Node(children: Vector[Node], accumulator: Accumulator)


object Main extends App {

  println ( "Start" )
  println ()

  // Get Currency Rates
  val address = "https://fx.priceonomics.com/v1/rates/"
  val response = Http ( address ).asString
  val jsonResponse = response.body.parseJson

  println ( s"Currency Rates: ${jsonResponse.prettyPrint}" )


  // Parse data
  var parsedData: Map[String, Map[String, BigDecimal]] = jsonResponse.convertTo [Map[String, String]]
    .map { case (currencyPair, rate) =>
      val from :: to :: Nil = currencyPair.split ( "_" ).toList
      RowData ( from, to, BigDecimal ( rate ) )
    }
    .groupBy ( _.from ).map { case (currencyFrom, array) =>
    currencyFrom -> array.map ( row => row.to -> row.rate ).toMap
  }


  // Calculation algorithm

  val results = new mutable.HashSet[Accumulator]()

  def loop(currentCurrency: String, acc: Accumulator)
          (implicit currency: String, parsedData: Map[String, Map[String, BigDecimal]]): Vector[Node] = {
    parsedData.get ( currentCurrency ) match {
      case Some ( children ) => children
                                .filter ( _._1 != currentCurrency ) // prevent to convert Same Currency to Same Currency
                                .map ( x => (x, Arrow ( currentCurrency, x._1 )) )
                                .filter ( x => !acc.arrows.contains ( x._2 ) ) // prevent loops
                                .map { case ((childCurrency, rate), arrow) =>
        val childAcc = acc.copy ( acc.rate * rate, acc.arrows :+ arrow )
        val children = if (currency == childCurrency) Vector.empty else loop ( childCurrency, childAcc )
        if (currency == childCurrency) // ugly side effect: collect all results
          results.add ( childAcc )
        Node ( children, childAcc )
                                }.toVector

      case _ => Vector.empty
    }
  }


  // Run calculation
  implicit val currencyToCheck = "USD"
  implicit val parsedDataset = parsedData

  loop ( currencyToCheck, Accumulator ( 1, Vector.empty ) )


  // Print results
  println ()
  println ( "Print Results:" )

  def Desc[T: Ordering] = implicitly [Ordering[T]].reverse

  results.toList.sortBy ( _.rate )( Desc ).foreach ( println )





  println ( "End" )
}


